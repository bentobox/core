@if ($paginator->hasPages())
    <ul class="controls scroll">
        {{-- Previous Page Link --}}
        <span class="left-arrow">
          @if ($paginator->onFirstPage())
              <li><i class="icon icon-arrow-l"></i></li>
          @else
              <li><a href="{{ $paginator->previousPageUrl() }}" rel="prev"><i class="icon icon-arrow-l"></i></a></li>
          @endif
        </span>

        {{-- Pagination Elements --}}
        <span class="page-numbers">
          @foreach ($elements as $element)
              {{-- "Three Dots" Separator --}}
              @if (is_string($element))
                  <li><i>{{ $element }}</i></li>
              @endif

              {{-- Array Of Links --}}
              @if (is_array($element))
                  @foreach ($element as $page => $url)
                      @if ($page == $paginator->currentPage())
                          <li class="active"><strong>{{ $page }}</strong></li>
                      @else
                          <li><a href="{{ $url }}">{{ $page }}</a></li>
                      @endif
                  @endforeach
              @endif
          @endforeach
        </span>

        {{-- Next Page Link --}}
        <span class="right-arrow">
          @if ($paginator->hasMorePages())
              <li><a href="{{ $paginator->nextPageUrl() }}" rel="next"><i class="icon icon-arrow-r"></i></a></li>
          @else
              <li><i class="icon icon-arrow-r"></i></li>
          @endif
        </span>
    </ul>
@endif