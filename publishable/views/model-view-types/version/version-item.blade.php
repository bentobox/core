@extends('bentobox::app')

<?php
	$sortedByPanel = [];
	foreach ($attributes as $attribute_name => $attribute) {
		if (array_key_exists('panel', $attribute)) {
			$sortedByPanel[$attribute['panel']][$attribute_name] = $attribute;
		} else {
			$sortedByPanel['uncategorized'][$attribute_name] = $attribute;
		}
	}
?>

@section('content')
  <h1>{{$model->id ? 'Edit ' . array_get($modelConfig, 'title') : 'New ' . array_get($modelConfig, 'title')}}</h1>

  <form class="form-container" method="POST" action="/{{config('bentobox.route_prefix', 'admin')}}/{{ array_get($modelConfig, 'url') }}/{{ $model->id }}" enctype="multipart/form-data">
    {{ csrf_field() }}

	{{-- Iterate through panels --}}
    @foreach ($sortedByPanel as $panel => $attributes)
		<div class="form-panel {{ $panel }}">

			@if ($panel !== 'uncategorized')
				<h3>{{ $panel }}</h3>
			@endif

			{{-- Iterate through attributes --}}
		    @foreach ($attributes as $attribute_name => $attribute_value)
	            @include('bentobox::shared.inputs.templates._' . $attribute_value['type'], ['attribute' => $attribute_value, 'model' => $model, 'attribute_name' => $attribute_name])
		    @endforeach

		</div>
    @endforeach

    <button class="primary-button update-create-button animating-button" type="submit"><i class="{{$model->id ? 'icon-refresh' : 'icon-Plus'}}"></i><span>{{$model->id ? 'Update' : 'Create'}}</span></button>
    <button class="floating-button update-create-button" type="submit"><i class="{{$model->id ? 'icon-refresh' : 'icon-Plus'}}"></i></button>
  </form>

  <br>
@endsection
