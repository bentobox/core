<?php
		$morphToMany = array_get($attribute, 'model');
		$morphToMany = $morphToMany::all();
		$option = array_get($attribute, 'attribute');

		$hydrationIds = $morphToMany->intersect($model->$attribute_name)->map(function($item) { return $item->id; });
?>
<div class="form-group multi-select">
		<label for="{{ $attribute_name }}" class="multi-label">
			@bentoLabel($attribute)
		</label>
		<select
			name="{{$attribute_name}}[]"
			id="{{$attribute_name}}"
			{{ array_get($attribute, "required") ? "required" : "" }} multiple="multiple">
				@foreach($morphToMany as $value)
						<option
							@if ($hydrationIds->contains($value->id))
								selected="selected"
							@endif
							value="{{ $value->id }}">
								{{ $value->$option }}
						</option>
				@endforeach
		</select>
		<input type="hidden" name="bentobox_relationship_morphToMany_{{ $attribute_name }}" value="{{ $attribute_name }}_morphToMany" />
		@include('bentobox::shared.inputs.errors._errors', ['errors' => $errors, 'attribute_name' => $attribute_name])
</div>

<script>
	document.multiselect('#{{$attribute_name}}');
</script>
