<header>
	<i class="ripple burger material-icons">menu</i>
	<h3 class="title"><a href="/{{config('bentobox.route_prefix', 'admin')}}">Bentobox</a></h3>
</header>
