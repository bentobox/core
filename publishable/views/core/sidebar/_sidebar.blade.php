<aside>
	<h3 class="logo">Bentobox</h3>
	<div class="models">
		<h4 class="sub-title
			@if(Request::is(config('bentobox.route_prefix', 'admin')))
				active
			@endif
		">
			<a class="dashboard" href="{{ url(config('bentobox.route_prefix', 'admin')) }}">
				<div class="clock">
					<svg viewBox="0 0 300 320" preserveAspectRatio="xMinYMin meet">
						<polygon class="hex" fill="transparent" points="300,150 225,280 75,280 0,150 75,20 225,20"></polygon>
						<line class="hours-hand" x1="150" y1="150" x2="150" y2="75" />
						<line class="minutes-hand" x1="150" y1="150" x2="150" y2="50" />
						{{-- <line class="seconds-hand" x1="150" y1="150" x2="150" y2="45" /> --}}
					</svg>
				</div>
				<span>Dashboard</span>
			</a>
		</h4>

		<h4 class="sub-title"><i class="icon-models-icon"></i>Models</h4>
		{!! Skyrkt\Bentobox\Services\SidebarService::printSidebarLayoutAsHtml() !!}
	</div>

	@if (count(config('bentobox.menu_items')) > 0)
		<hr>
		<ul class="menu-items">
			@foreach (config('bentobox.menu_items') as $menu_item_label => $menu_item_link)
			<li class="
				@if (Request::path() === $menu_item_link)
					active
				@endif
			">
				<a href="{{url($menu_item_link)}}">{{$menu_item_label}}</a>
			</li>
			@endforeach
		</ul>
	@endif
	<div class="logout">
		<a href="{{ route('bento_logout') }}" class="h6"><i class="icon-logout-rounded-down"></i>Logout</a>
	</div>
</aside>
