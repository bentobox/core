export class File {
  constructor(elements) {
    this.elements = Array.prototype.slice.call(elements);
    this.setListenersOnElements(this.elements);
  }

  setListenersOnElements(elements) {
    elements.map(element => {
      const input = element.querySelector('input');
      const button = element.querySelector('button');

      button.addEventListener('click', () => { input.click() });
      input.addEventListener('change', () => { this.checkState(element, input) });
    });
  }

  checkState(element, input) {
    if (input.files.length > 0) {

      const file = input.files[0];
      this.handleMessage(element, file.name);

      if (this.canPreview(file)) {

        this.getFileDataUrl(file, dataUrl => {
          this.handlePreview(element, dataUrl);
        });

      } else {

        this.removePreview(element);

      }
    } else {

      this.handleMessage(element, '(No file chosen)');

    }
  }

  getFileDataUrl(file, cb) {
    const reader = new FileReader();

    reader.onload = e => {
      cb(e.target.result);
    };

    reader.readAsDataURL(file);
  }

  handleMessage(element, message) {
    const messageElement = element.querySelector('.message');
    messageElement.innerHTML = message;
  }

  handlePreview(element, dataUrl) {
    const preview = element.querySelector('.preview');
    preview.style.display = 'block';
    preview.innerHTML = `<img src="${dataUrl}">`;
  }

  removePreview(element) {
    const preview = element.querySelector('.preview');
    preview.style.display = 'none';
    preview.innerHTML = '';
  }

  canPreview(file) {
    return file.type.match(/image\/.*/g);
  }
}
