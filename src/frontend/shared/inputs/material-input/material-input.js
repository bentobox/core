import { Validator } from '../errors/validator';

export class MaterialInput {
  constructor(elements) {
    this.validator = new Validator();
    this.elements = Array.prototype.slice.call(elements);
    this.setListenersOnElements(this.elements);
  }

  setListenersOnElements(elements) {
    elements.map(element => {
      const input = this.getInput(element);
      this.handleDate(element, input);
      this.checkState(element, input);
      input.addEventListener('focus', () => this.checkState(element, input));
      input.addEventListener('blur', () => this.checkState(element, input));
      input.addEventListener('input', () => this.checkState(element, input));
    })
  }

  getInput(element) {
    if (element.querySelector('input')) return element.querySelector('input');
    if (element.querySelector('select')) return element.querySelector('select');
    if (element.querySelector('textarea')) return element.querySelector('textarea');
  }

  checkState(element, input) {
    this.handleErrors(element, input);

    (this.focusStateCheck(input)) ?
      this.handleFocus(element) : this.handleBlur(element);

    (this.valueStateCheck(input)) ?
      this.handleValue(element) : this.handleNoValue(element);
  }

  focusStateCheck(input) {
    return input === document.activeElement;
  }

  valueStateCheck(input) {
    return input.value.length > 0;
  }

  handleFocus(element) {
    element.classList.add('is-focussed');
  }

  handleBlur(element) {
    element.classList.remove('is-focussed');
  }

  handleValue(element) {
    element.classList.add('has-value');
  }

  handleNoValue(element) {
    element.classList.remove('has-value');
  }

  handleErrors(element, input) {
    element.classList.remove('is-errored');
    const errorElem = element.querySelector('.errors');

    if (errorElem) {
      this.cleanErrorList(errorElem);

      const errors = this.validator.validate(element, input);

      if (errors.length > 0) {
        element.classList.add('is-errored');
        errors.map(error => {
          errorElem.innerHTML += `<li class="error js">${error}</li>`
        })
      }
    }
  }

  cleanErrorList(errorElem) {
    Array.prototype.forEach.call(errorElem.querySelectorAll('.js'), error => {
      error.parentNode.removeChild(error);
    });
  }

  handleDate(element, input) {
    if (input.attributes.type && input.attributes.type.value === 'date') {
      element.classList.add('is-date');
    }
  }
}
