import './app.css';

import {
  Alert,
  Header,
  Clock
} from './core';

import {
  Table
} from './model-view-types';

import {
  AnimatingButton,
  MaterialInput,
  File
} from './shared';

// // this toggles sidebar
const headerElement = document.querySelector('header');
const sidebarElement = document.querySelector('aside');
if (headerElement && sidebarElement) {
  new Header(headerElement, sidebarElement);
}

const tableElement = document.querySelector('.table-table');
if (tableElement) {
  const tableControl = document.querySelector('.table-control');
  new Table(tableElement, tableControl);
}

const alertElement = document.querySelector('.alert');
if (alertElement) {
  new Alert(alertElement);
}

const materialElements = document.querySelectorAll('.material');
if (materialElements.length > 0) {
  new MaterialInput(materialElements);
}

const animatingButton = document.querySelector('.animating-button');
if (animatingButton) {
  new AnimatingButton(animatingButton);
}

const fileInputs = document.querySelectorAll('.file-input');
if (fileInputs.length > 0) {
  new File(fileInputs);
}

const clock = document.querySelector('.clock');
if (clock) {
  new Clock(clock);
}

