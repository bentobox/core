@extends('bentobox::app')

@section('content')
  <div class="table-header">
    <h1 class="title h2">{{array_get($modelConfig, 'title')}}</h1>
    <a href="{{url(config('bentobox.route_prefix', 'admin') . '/' . array_get($modelConfig, 'url') . '/new')}}"><button class="primary-button update-create-button animating-button"><i class="icon-Plus"></i><span>New {{array_get($modelConfig, 'title')}}</span></button></a>
    <a href="{{url(config('bentobox.route_prefix', 'admin') . '/' . array_get($modelConfig, 'url') . '/new')}}"><button class="floating-button update-create-button"><i class="icon-Plus"></i></button></a>
  </div>
  <section class="form-container">
    <div class="form-panel">
      <ul class="table-control controls flex scroll">
      	<span class="select-all-box">
  			<li><input class="select-all" type="checkbox" class="select-all" name="select-all"></li>
  		</span>

        <span>
      		<li><a class="delete-action" href="#">Delete</a></li>
        </span>

        <span>
          <li>{{-- <label for="search"><i class="icon icon-search"></i></label><input type="text" name="search" id="search" placeholder="Search"> --}}</li>
        </span>
      </ul>
      @include('bentobox::model-view-types.table.main.table._table-table')
    </div>
  </section>

<div class="table-pagination">
  {{ $pagination }}
</div>

<form class="delete-form" method="POST" action="/{{config('bentobox.route_prefix', 'admin')}}/{{ array_get($modelConfig, 'url') }}">
  {{ csrf_field() }}
  <input type="hidden" id="ids_to_delete" name="ids_to_delete" value="">
  <input name="_method" type="hidden" value="DELETE">
</form>
@endsection

