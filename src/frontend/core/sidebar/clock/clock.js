export class Clock {
  constructor(element) {
    this.hoursHand = element.querySelector('.hours-hand');
    this.minutesHand = element.querySelector('.minutes-hand');
    this.secondsHand = element.querySelector('.seconds-hand');
    const date = new Date();

    this.applyTime(date.getHours(), date.getMinutes(), date.getSeconds());
    this.startListeningForTime();
  }

  startListeningForTime() {
    setInterval(() => {
      const date = new Date();
      this.diffTime(
        date.getHours(),
        date.getMinutes(),
        date.getSeconds()
      )
    }, 1000);
  }

  diffTime(hours, minutes, seconds) {
    if (
      // this.seconds !== seconds ||  Seconds b-line it for the obvious, don't like that
      this.minutes !== minutes ||
      this.hours !== hours // I don't think hours will ever trigger this?
    ) {
      this.applyTime(hours, minutes, seconds);
    }
  }

  applyTime(hours, minutes, seconds) {
    this.hours = hours;
    this.minutes = minutes;
    this.seconds = seconds;

    this.setMinuteHand(this.minutes);
    this.setHourHand(this.hours, this.minutes);
    // this.setSecondsHand(this.seconds);
  }

  setMinuteHand(minutes) {
    const degree = (minutes * 360) / 60;
    this.minutesHand.style.transform = `rotate(${degree}deg)`;
  }

  setHourHand(hours, minutes) {
    hours = ((hours + 11) % 12 + 1);
    let degree = (hours * 360) / 12;
    degree += (minutes * 30) / 60;
    this.hoursHand.style.transform = `rotate(${degree}deg)`;
  }

  setSecondsHand(seconds) {
    const degree = (seconds * 360) / 60;
    this.secondsHand.style.transform = `rotate(${degree}deg)`;
  }
}
