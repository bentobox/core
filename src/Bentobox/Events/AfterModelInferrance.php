<?php

namespace Skyrkt\Bentobox\Events;

use Illuminate\Queue\SerializesModels;

class AfterModelInferrance
{
    use SerializesModels;

    public $inferredModels;

    /**
     * Create a new event instance.
     *
     * @param Collection $inferredModels
     * @return void
     */
    public function __construct($inferredModels)
    {
    	$this->inferredModels = $inferredModels;
    }
}
