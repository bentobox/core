<?php

namespace Skyrkt\Bentobox\Listeners;

use Illuminate\Support\Facades\Validator;
use Skyrkt\Bentobox\Listeners\BentoboxModelHandler;
use App;

class ValidateModel extends BentoboxModelHandler
{
	/**
	 * Validaton rules
	 *
	 * @var array
	 */
	private $rules = [];

	/**
	 * Authorization
	 *
	 * @var boolean
	 */
	private $authorize = true;

	/**
	 * Initialize rules and fire validation
	 *
	 * @return void
	 */
	protected function fire()
	{

		if (isset($this->model->bentobox_request)) {
			$this->request = App::make($this->model->bentobox_request);
			return true;

			} else {
				$this->rules = $this->model->bentobox_rules ?
					$this->model->bentobox_rules : [];

				if (method_exists($this->model, 'bentobox_authorize')) {
					$this->authorize = $this->model->bentobox_authorize();
				}

				return $this->authorize();
			}
	}

	/**
	 * Determine if this model action may be run by the
	 * current user.
	 *
	 * @return bool
	 */
	private function authorize()
	{
		if ($this->authorize === false) {
			return false;

		} else {
			return $this->validate();
		}
	}

	/**
	 * Validate the model
	 *
	 * @return boolean
	 */
	private function validate()
	{
		if (! empty($this->request->all()) && ! empty($this->rules)) {
			$validator = Validator::make($this->request->all(), $this->rules);

			if (! $validator->passes()) {
				$this->model->bento_errors = $validator->messages();
				return false;
			}
		}
	}
}
