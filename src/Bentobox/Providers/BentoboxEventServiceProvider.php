<?php

namespace Skyrkt\Bentobox\Providers;

use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Event;
use Skyrkt\Bentobox\Events\BentoboxModelSaved;
use Skyrkt\Bentobox\Events\BentoboxModelSaving;
use Skyrkt\Bentobox\Listeners\ValidateModel;
use Skyrkt\Bentobox\Listeners\StoreModelMedia;

class BentoboxEventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();

        /**
         * Listen for all eloquent models attempting to save and
         * and let Bentobox handle any tasks which may be required.
         */
        Event::listen(['eloquent.saving: *'],
            'Skyrkt\Bentobox\Listeners\ValidateModel');

        Event::listen(['eloquent.creating: *'],
            'Skyrkt\Bentobox\Listeners\InsertSortValues');

        /**
         * Listen for all eloquent models being saved and let Bentobox
         * handle any tasks which may be required on them.
         */
        Event::listen(['eloquent.saved: *'],
            'Skyrkt\Bentobox\Listeners\StoreModelMedia');

		Event::listen(['Skyrkt\Bentobox\Events\AfterModelInferrance'],
            'Skyrkt\Bentobox\Listeners\ModelViewTypeMutation');
    }
}
