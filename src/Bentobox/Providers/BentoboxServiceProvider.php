<?php

namespace Skyrkt\Bentobox\Providers;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Blade;
use Illuminate\Support\Facades\Event;
use Illuminate\Support\ServiceProvider;
use Skyrkt\Bentobox\Services\Inferrer;

class BentoboxServiceProvider extends ServiceProvider {

  /**
   * Bootstrap the application events.
   *
   * @return void
   */
  public function boot()
  {
    // Load our provided routes.php as long as routes aren't being cached
    if (! $this->app->routesAreCached()) {
      require __DIR__.'/../../routes/routes.php';
    }

    // Load our views with the namespace bentobox
    $this->loadViewsFrom(__DIR__.'/../../frontend', 'bentobox');

    // Load our migrations commented out so migrations dont fail.
    $this->loadMigrationsFrom(__DIR__.'/../../database/migrations');

    // Advertise that this has a config file called bentobox.php
    $this->publishes([
      __DIR__.'/../../config/bentobox.php' => config_path('bentobox.php')
    ], 'config');

    $this->publishes([
      __DIR__.'/../../../publishable/public' => public_path('vendor/bentobox')
    ], 'public');

    $this->publishes([
      __DIR__.'/../../../publishable/views' => resource_path('views/vendor/bentobox')
    ], 'views');

    Inferrer::models();


    /**
     * TODO: Extract these out to blade partials instead of directives.
     * If someone chooses to publish the views in the package, they won't be able to
     * override how labels and attribute content is supplied and produced. -Ben
     */
    Blade::directive('bentoLabel', function($attribute) {
      return "{{array_get($attribute, 'label')}} {{ array_get($attribute, 'required') ? '*' : '' }}";
    });

    Blade::directive('bentoInput', function($attribute) {
      return 'name="{{$attribute_name}}" id="{{$attribute_name}}" value="{{ array_get($model, $attribute_name, null) }}" {{ array_get($attribute, "required") ? "required" : "" }}';
    });
  }

  /**
   * Register the service provider.
   *
   * @return void
   */
  public function register()
  {
    $this->app->register(\Intervention\Image\ImageServiceProvider::class);
    $this->app->register(\Skyrkt\Bentobox\Providers\BentoboxEventServiceProvider::class);

    $loader = \Illuminate\Foundation\AliasLoader::getInstance();
    $loader->alias('Image', \Intervention\Image\Facades\Image::class);
  }

/**
   * Get the services provided by the provider.
   *
   * @return array
   */
  public function provides()
  {
      return [
        Inferrer::class
      ];
  }
}
