<?php

namespace Skyrkt\Bentobox\Services;

use Image;
use Skyrkt\Bentobox\Services\MediaHandler;
use Storage;

class ImageService implements MediaHandler
{
	/**
	 * Default settings if no transformations are passed in
	 *
	 * @var array
	 */
	protected $defaults = [
		'width' => null,
		'height' => null,
		'orientate' => true,
		'constrainUpsize' => true,
		'constrainAspectRatio' => true,
		'transform_name' => null,
		'fit' => false
	];
	/**
	 * Image having transformations run against it
	 *
	 * @var UploadedFile
	 */
	protected $image;

	/**
	 * Array of transformations to be applied to the image
	 *
	 * @var array
	 */
	protected $transformations;

	/**
	 * Build up image class and pass in optional transformation configuration
	 *
	 * @param file     $image
	 * @param array|null $transformations
	 */
	public function __construct($image = null, array $transformations = [])
	{
		$this->image = $image;
		$this->transformations = $this->setTransformations($transformations);
	}

	/**
	 * Process the image
	 *
	 * @return Object
	 */
	public function handle()
	{
		$t = $this->transformations;

		// TODO: THIS POOP IS A TEMP FIX FOR SVG UPLOADING. I KNOW,
		// I KNOW, IT"S TERRIBLEEEFSJKDERFGKSJDF!
		if ($this->image->guessExtension() === 'svg') {
			return [
				'image' => $image,
				'ext' => $this->image->guessExtension(),
				'transform_name' => $t['transform_name']
			];
		}

		$image = Image::make($this->image);

		if ($t['orientate']) $image->orientate();

		if ($t['width'] || $t['height'])
		{
			if ($t['fit']) {
				$image->fit($t['width'], $t['height'], function ($c) use ($t) {
					if ($t['constrainUpsize']) $c->upsize();
					if ($t['constrainAspectRatio']) $c->aspectRatio();
				});
			} else {
				$image->resize($t['width'], $t['height'], function ($c) use ($t) {
					if ($t['constrainUpsize']) $c->upsize();
					if ($t['constrainAspectRatio']) $c->aspectRatio();
				});
			}

		}

		$image->encode($this->image->guessExtension());

		return [
			'image' => $image,
			'ext' => $this->image->guessExtension(),
			'transform_name' => $t['transform_name']
		];
	}

	/**
	* Build up our transformation options
	*
	* @param  array $settings
	* @return array
	*/
	private function setTransformations($settings)
	{
		$parsedTransformations = $this->defaults;

		foreach ($settings as $label => $value) {
			$parsedTransformations = array_merge($parsedTransformations,
				[$label => $value]);
		}

		return $parsedTransformations;
	}
}
