<?php

namespace Skyrkt\Bentobox\Services;

interface MediaHandler
{
	public function handle();
}
