<?php

namespace Skyrkt\Bentobox\Services;
use App;
use Skyrkt\Bentobox\Services\Inferrers\AttributeConfigurationInferrer;
use Skyrkt\Bentobox\Services\Inferrers\ModelInferrer;
use Skyrkt\Bentobox\Services\Inferrers\ShouldIncludeInferrer;

/**
 * Used as an entry point to all the inferences that Bentobox needs to make about models,
 * configurations, and attributes.
 */
class Inferrer {

	/**
	 * When Inferrer gets loaded into the Service Container, new up some of it's Inferrer classes
	 */
	public function __construct()
	{
		App::bind('Skyrkt\Bentobox\Services\Inferrer\AttributeConfigurationInferrer', function() {
			return new AttributeConfigurationInferrer();
		});

		App::bind('Skyrkt\Bentobox\Services\Inferrer\ModelInferrer', function() {
			return new ModelInferrer();
		});

		App::bind('Skyrkt\Bentobox\Services\Inferrer\ShouldIncludeInferrer', function() {
			return new ShouldIncludeInferrer();
		});
	}

	/**
	 * Infer the configuration of the model based on it's fillable, and bentobox_attributes arrays
	 * @param  Model $model The eloquent model
	 * @return array        the inferred configuration
	 */
	public static function attributeConfig($model)
	{
		$inferrer = App::make(AttributeConfigurationInferrer::class);
		return $inferrer->makeAttributeConfigFromModel($model);
	}

	/**
	 * Infer the models from the applcation.
	 * @return collection of inferred models
	 */
	public static function models()
	{
		$inferrer = App::make(ModelInferrer::class);
		return $inferrer->getModelsFromApplication();
	}

	/**
	 * Use the attributes on the model and in global configuration to
	 * see if the model should be checked or not.
	 * @param  Model $model the model to check
	 * @return boolean
	 */
	public static function shouldIncludeModel($model)
	{
		$inferrer = App::make(ShouldIncludeInferrer::class);
		return $inferrer->shouldIncludeModel($model);
	}

}
