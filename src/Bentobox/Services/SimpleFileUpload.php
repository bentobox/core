<?php
/**
 * Force build
 */
namespace Skyrkt\Bentobox\Services;

use Illuminate\Http\Request;

class SimpleFileUpload
{
	/**
	 * Upload a file and merge the path string to the request
	 * array
	 *
	 * @param  Request $request
	 * @param  array  $config
	 * @return Illuminate\Http\Request
	 */
	public static function upload(Request $request, $config)
	{
		foreach ($config as $key => $value)
		{
			$type = array_get($value, 'type');

			$disk = array_get($value, 'disk') ?
				array_get($value, 'disk') : 'public';

			$filepath = array_get($value, 'filepath') ?
				array_get($value, 'filepath') : 'files';

			$filename = array_get($value, 'filename') ?
				array_get($value, 'filename') : date('Y-m-d-h-i-s');

			if ($type === 'file' && $request->hasFile('bentobox_' . $key)) {
				$file = $request->file('bentobox_' . $key);
				$extension = $file->guessExtension();
				$path = $file->storeAs($filepath, $filename .'.'. $extension, $disk);

				return $request->merge([$key => $path]);
			}
		}
	}
}
