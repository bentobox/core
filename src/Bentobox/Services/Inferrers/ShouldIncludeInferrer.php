<?php

namespace Skyrkt\Bentobox\Services\Inferrers;

use Illuminate\Database\Eloquent\Model;
use App;

class ShouldIncludeInferrer {

	/**
	 * Use the attributes on the model and in global configuration to
	 * see if the model should be checked or not.
	 *
	 * This chain of checks could be a good example of where a
	 * "chain of responsibility" pattern would come in.
	 *
	 * @param  Model $model the model to check
	 * @param  Array $modelConfig the model config of the model, optional
	 * @return boolean
		*/
	public function shouldIncludeModel($model, $modelConfig = null)
	{
		$modelConfig = ($modelConfig === null) ? $this->getModelConfig($model) : $modelConfig;

		$disabled = $model->bentobox_disabled || $modelConfig['disabled'];
		$enabled = $model->bentobox_enabled || $modelConfig['enabled'];
		$toggle = $model->bentobox_toggle || $modelConfig['toggle'];
		$inclusionMode = config('bentobox.manual_inclusion_mode');

		if (!is_subclass_of($model, Model::class)) {
			return false;

		} elseif ($disabled === true) {
			return false;

		} elseif ($enabled === true) {
			return true;

		} elseif ($inclusionMode === true && $toggle === false) {
			return false;

		} elseif ($inclusionMode === false && $toggle === true) {
			return false;

		}
		return true;
	}

	private function getModelConfig($model)
	{
		$modelInferrer = App::make(\Skyrkt\Bentobox\Services\Inferrers\ModelInferrer::class);

		return $modelInferrer->mergeConfigWithDefault($model);
	}
}
