<?php

namespace Skyrkt\Bentobox\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Storage;

class MediaController extends BaseController {
	/**
	* Return the index view for the Bentobox CMS
	* @return [type] [description]
	*/
	public function uplooadImageFromWYSIWYG(Request $request, $model)
	{
		$disk = 's3';
		$filepath = 'files';
		$filename = date('Y-m-d-h-i-s') . '-' . $model . '-' . '-tinyMCE-up';

		$file = $request->file('file');
		$extension = $file->guessExtension();
		$path = $file->storeAs($filepath, $filename .'.'. $extension, $disk);

		return Storage::disk($disk)->url($path);
	}
}
