<?php

namespace Skyrkt\Bentobox\Http\Controllers\ModelViewTypes;

/**
 * This is the contract that ModelViewTypes need to adhere to.
 *
 * If making a new ModelViewType you need extend this class, and adhere to the naming standard as defined in `ModelController`.
 */
abstract class ModelViewType {

	protected $modelClass;
	protected $modelConfig;
	protected $modelAttributeConfig;

	public function __construct($modelClass, $modelConfig, $modelAttributeConfig)
	{
		$this->modelClass = $modelClass;
		$this->modelConfig = $modelConfig;
		$this->modelAttributeConfig = $modelAttributeConfig;
	}

	public abstract function index($request);
	public abstract function show($request, $id);
	public abstract function store($request, $id);
	public abstract function delete($request, $id);
}
