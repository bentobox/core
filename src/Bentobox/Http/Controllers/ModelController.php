<?php

namespace Skyrkt\Bentobox\Http\Controllers;

use App;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Route;
use Skyrkt\Bentobox\Services\Inferrer;
use Skyrkt\Bentobox\Http\Controllers\ModelViewTypes\ModelViewType;

/**
 * This class serves as the entrypoint to Bentobox's admin panel.
 *
 * The steps from here are as follows
 * - Look at the url parameter for a model that matches that url (url generation comes from StringHelper class).
 * - With the matching model we can infer it's configuration and full classReference
 * - Now that we have the configuration we can new up a new "ModelViewType". "ModelViewType" is the language Bentobox uses to
 * differenciate between different structures of data. So, for example, comments would be a "Table", while a homepage, that only has one record would be a "Single". This is Collection by default but can be set in the bentobox configuration file
 * - Once we have that new ModelViewType bind it into the ServiceContainer.
 * - From there we delegate our CRUD tasks to that ModelViewType, so we can have different methods for different ModelViewTypes.
 */
class ModelController extends BaseController
{

	/**
	 * Namespace of where ModelViewType classes are located
	 * @var string
	 */
	protected $BENTOBOX_MODEL_PRESENTATION_TYPE_NAMESPACE = 'Skyrkt\Bentobox\Http\Controllers\ModelViewTypes\\';

	/**
	 * Gets information about what model we're dealing with
	 * and what TypeService we need to delegate actions to.
	 */
	public function __construct()
	{
		$model_url = Route::current()->parameter('model');
		$modelClassAndConfig = $this->getModelClassAndConfigFromUrl($model_url);

		$modelClass = $modelClassAndConfig['class'];
		$modelConfig = $modelClassAndConfig['config'];
		$modelAttributeConfig = Inferrer::attributeConfig(new $modelClass());

		// Decide what type engine to use, based on model name and type.
	// `$modelConfig['type']` is here twice because it's once for folder name, then once for name of file
	// So if making a new type make sure you follow that folder/naming structure.
		$modelViewTypeClass = $this->BENTOBOX_MODEL_PRESENTATION_TYPE_NAMESPACE . $modelConfig['type'] . '\\' . $modelConfig['type'];

		// Bind that engine into the service container.
		App::bind($this->BENTOBOX_MODEL_PRESENTATION_TYPE_NAMESPACE . 'ModelViewType', function() use ($modelClass, $modelConfig, $modelAttributeConfig, $modelViewTypeClass) {
			return new $modelViewTypeClass($modelClass, $modelConfig, $modelAttributeConfig);
		});
	}

	public function index(Request $request, ModelViewType $modelViewType)
	{
		return $modelViewType->index($request);
	}

	public function show(Request $request, ModelViewType $modelViewType, $model_url, $id = null)
	{
		return $modelViewType->show($request, $id);
	}

	public function store(Request $request, ModelViewType $modelViewType, $model_url, $id = null)
	{
		return $modelViewType->store($request, $id);
	}

	public function delete(Request $request, ModelViewType $modelViewType, $model_url, $id = null)
	{
		return $modelViewType->delete($request, $id);
	}

	private function getModelClassAndConfigFromUrl($modelUrl)
	{
		$classAndConfig = Inferrer::models()->filter(function($model) use ($modelUrl) {
			return $model['url'] === $modelUrl;
		});
		$modelClass = $classAndConfig->keys()[0];
		return [
			'class' => $modelClass,
			'config' => $classAndConfig->get($modelClass)
		];
	}
}
