<?php

namespace Skyrkt\Bentobox\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;

class DashboardController extends BaseController {
	/**
	* Return the index view for the Bentobox CMS
	* @return [type] [description]
	*/
	public function index()
	{
		// There needs to be a permission check on the dashboard.
		return view('bentobox::dashboard.dashboard');
	}
}
