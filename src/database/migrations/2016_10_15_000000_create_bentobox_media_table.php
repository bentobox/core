<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBentoboxMediaTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bentobox_media', function (Blueprint $table) {
            $table->increments('id');
            $table->string('disk')->nullable();
            $table->string('attribute')->nullable();
            $table->string('transform_name')->nullable();
            $table->string('title')->nullable();
            $table->string('alt')->nullable();
            $table->string('url')->nullable();

            $table->string('model_id');
            $table->string('model_type');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('bentobox_media');
    }
}
