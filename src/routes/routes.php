<?php

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['web']], function () {
	Route::group(['middleware' => ['auth'], 'prefix' => config('bentobox.route_prefix', 'admin')], function() {
		Route::get('logout', function() { Auth::logout(); return redirect('/'); })->name('bento_logout');

		Route::post('{model}/image',  'Skyrkt\Bentobox\Http\Controllers\MediaController@uplooadImageFromWYSIWYG');

		Route::get('',                     'Skyrkt\Bentobox\Http\Controllers\DashboardController@index');
		Route::get('{model}',              'Skyrkt\Bentobox\Http\Controllers\ModelController@index');
		Route::get('{model}/new',          'Skyrkt\Bentobox\Http\Controllers\ModelController@show');
		Route::get('{model}/{id}',         'Skyrkt\Bentobox\Http\Controllers\ModelController@show');
		Route::post('{model}',             'Skyrkt\Bentobox\Http\Controllers\ModelController@store');
		Route::post('{model}/{id}',        'Skyrkt\Bentobox\Http\Controllers\ModelController@store');
		Route::delete('{model}/{id?}',     'Skyrkt\Bentobox\Http\Controllers\ModelController@delete');
	});
});
