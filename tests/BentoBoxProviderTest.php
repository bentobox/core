<?php
namespace Skyrkt\Bentobox\Tests;

use Mockery as m;
use Illuminate\Contracts\Foundation\Application;
use \Skyrkt\Bentobox\BentoboxProvider;

/**
 * This class is used as a very silly example of how to use
 * mockery and the PHPUnit in general.
 */
class TestTest extends \PHPUnit_Framework_TestCase {

	/**
	 * The Bentobox ServiceProvider Glue For Laravel
	 * @var BentoboxProvider
	 */
	protected $serviceProvider;

	/**
	 * Run before the tests are executed in this class
	 */
	public function setUp()
	{
		$app = m::mock(Application::class);
		$this->serviceProvider = new BentoboxProvider($app);
	}

	/** @test */
	public function it_runs_get_twelve_and_expects_twelve()
	{
		$this->assertEquals($this->serviceProvider->getTwelve(), 12);
	}

}
