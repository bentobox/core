# Bento Box
###### A Drop-in Admin CMS Package for Your Laravel 5 Projects.

![Bento Box](http://i.imgur.com/5Uaf3Fx.png)

[packagist](https://packagist.org/packages/bentobox/core)

### Bentobox Pre-Alpha Testing Setup Instructions

1. Create a new laravel project `laravel new TestBentobox` and cd into the dir
2. Install the latest dev package: `composer require bentobox/core dev-master`
3. Update `config/app.php` providers array to include `Skyrkt\Bentobox\Providers\BentoboxProvider::class,`
4. Create SQlite DB to test with `touch database/database.sqlite`
5. Set `.env` file to use sqlite instead of mysql. Remove `DB_DATABSE`, `DB_USERNAME`, `DB_PASSWORD` from file
6. Run `php artisan migrate` to migrate the default migrations as well as Bentobox media table.
7. Run 'php artisan make:auth' to set up Laravel's default Auth path
8. Run `php artisan vendor:publish --tag=config --tag=public --tag=icon --tag=views` to access the configs in your config directory and publish the styles, icons and views.
9. Fire up `php artisan serve` and create an account, the default admin panel can be located at the `/admin` route.
10. From here, try adding new migrations and models. `config/bentobox.php` and `vendor/skyrkt/bentobox/src/bentobox/models/BentoboxModel.php` contain instruction on how you can manage your model in the CMS.
